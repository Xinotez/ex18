#include "Helper.h"


void Helper::trim(string &str)
{
	rtrim(str);
	ltrim(str);

}

void Helper::rtrim(string &str)
{
	size_t endpos = str.find_last_not_of(" \t");
	if (std::string::npos != endpos)
	{
		str = str.substr(0, endpos + 1);
	}
}

void Helper::ltrim(string &str)
{
	size_t startpos = str.find_first_not_of(" \t");
	if (std::string::npos != startpos)
	{
		str = str.substr(startpos);
	}
}

vector<string> Helper::get_words(string &str)
{
	vector<string> words;
	std::istringstream strStream(str);
	copy(istream_iterator<string>(strStream),
		istream_iterator<string>(),
		back_inserter(words));

	return words;
}

